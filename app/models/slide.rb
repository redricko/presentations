class Slide < ActiveRecord::Base
	belongs_to :presentation

	validates :name, presence: true
	validates :presentation_id, presence: true
end
