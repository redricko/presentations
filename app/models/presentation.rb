class Presentation < ActiveRecord::Base
	has_many :slides

	validates :name, presence: true
end
