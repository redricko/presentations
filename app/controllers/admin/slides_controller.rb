module Admin
	class SlidesController < AdminController
		

		def new
	    @slide = Slide.new
	    @slide.presentation_id = params[:presentation_id]
	  end

	  def edit
	  	@slide = Slide.find(params[:id])
	  end

	  # def show
	  # 	@Slide = Slide.find(params[:id])
	  # end

	  def create
	    @slide = Slide.new(slide_params)

	    if @slide.save
	      redirect_to admin_presentation_path(@slide.presentation.id), notice: 'Slide was successfully created.'
	    else
	      render action: 'new'
	    end
	  end

	  def update
	  	@slide = Slide.find(params[:id])

	    if @slide.update(slide_params)
	      redirect_to admin_presentation_path(@slide.presentation.id), notice: 'Slide was successfully updated.'
	    else
	      render action: 'edit'
	    end
	  end

	  def destroy
	  	detail_id = @slide.presentation.id
	  	@slide = Slide.find(params[:id])

	    @slide.destroy
	    redirect_to admin_presentation_url(detail_id), notice: 'Slide was successfully destroyed.'
	  end

	  private

	    def slide_params
	      params.require(:slide).permit(:css_id, :sequence, :css_class, :name, :content, :presentation_id, :x, :y, :z, :rotate, :scale)
	    end
	end
end