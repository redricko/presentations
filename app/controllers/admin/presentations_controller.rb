module Admin
	class PresentationsController < AdminController
		
		def index
			@presentations = Presentation.all
		end

		def new
	    @presentation = Presentation.new
	  end

	  def edit
	  	@presentation = Presentation.find(params[:id])
	  end

	  def show
	  	@presentation = Presentation.find(params[:id])
	  end

	  def create
	    @presentation = Presentation.new(presentation_params)

	    if @presentation.save
	      redirect_to admin_presentation_path(@presentation), notice: 'Presentation was successfully created.'
	    else
	      render action: 'new'
	    end
	  end

	  def update
	  	@presentation = Presentation.find(params[:id])

	    if @presentation.update(presentation_params)
	      redirect_to admin_presentation_path(@presentation), notice: 'Presentation was successfully updated.'
	    else
	      render action: 'edit'
	    end
	  end

	  def destroy
	  	@presentation = Presentation.find(params[:id])

	    @presentation.destroy
	    redirect_to admin_presentations_url, notice: 'Presentation was successfully destroyed.'
	  end

	  private

	    def presentation_params
	      params.require(:presentation).permit(:name, :slug)
	    end
	end
end