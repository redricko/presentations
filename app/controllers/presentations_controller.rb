class PresentationsController < ApplicationController
	def index
		@presentations = Presentation.all
	end

	def show
		presentation = Presentation.find_by_slug(params[:slug])
		@slides = presentation.slides.order('sequence ASC')
	end
end
