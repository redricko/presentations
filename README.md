# Impress.js template

My template, which is just the upgraded version of [Chris Caselases](https://github.com/caselas) repository [impress-on-rails](https://github.com/caselas/impress-on-rails)

Rails is now on version 4.0.0

Whole presentation template deployed to [heroku](http://php-testing.antasandrej.net/)

### TODO:
Make it universal template
Generate slides on the go
Create admin interface
insert slides into DB
Create default slide types
:)