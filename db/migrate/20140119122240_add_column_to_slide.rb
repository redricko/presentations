class AddColumnToSlide < ActiveRecord::Migration
  def change
    add_reference :slides, :presentation, index: true
  end
end
