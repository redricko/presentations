class AddColumnsToSlide < ActiveRecord::Migration
  def change
    add_column :slides, :x, :string
    add_column :slides, :y, :string
    add_column :slides, :z, :string
    add_column :slides, :rotate, :string
    add_column :slides, :scale, :string
  end
end
