class CreateSlide < ActiveRecord::Migration
  def change
    create_table :slides do |t|
      t.string :css_id
      t.string :css_class
      t.string :name
      t.text :content

      t.timestamps
    end
  end
end
