class AddColumnSlugToPresentation < ActiveRecord::Migration
  def change
    add_column :presentations, :slug, :string
  end
end
