class AddColumnSequenceToSlide < ActiveRecord::Migration
  def change
    add_column :slides, :sequence, :integer
  end
end
